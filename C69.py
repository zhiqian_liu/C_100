"""有n个人围成一圈，顺序排号。
从第一个人开始报数（从1到3报数），凡报到3的人退出圈子，
问最后留下的是原来第几号的那位"""
def get_the_last(n):
    cnt=0
    ls=[i+1 for i in range (n)] #快速创建列表，值（初始编号）从1开始，下标从0开始
    while len(ls)>1: #列表中只剩最后一个值时返回
        ls_=ls[:] #将ls内容复制到ls_
        for j in range (len(ls_)):
            cnt+=1 #相当于指针
            if cnt%3==0:
                ls.remove(ls_[j])#这里只能使用remove，根据内容删除
    return ls[0]

n=int(input("请输入游戏人数："))
print("最后剩下的人是第%d号"%get_the_last(n))

