"""进制转换"""
#八进制转换为十进制
#常规思想
def octal_to_decimal(n):
    deci=0
    for i in range (len(n)):
        deci+=int(n[i])*pow(8,len(n)-1-i)
    return deci

n=str(input("请输入一个八进制数："))
print("对应的十进制数为%d"%octal_to_decimal(n))

