#判断101到200之间的素数
import math     #下面用到了math.sqrt()
def get_prime(x,y):
    ls=[]
    for i in range(x,y+1):
        k=True      #这里k的设置是必不可少的
        cnt=0
        for j in range(2,int(math.sqrt(i))+1):
            if i%j==0:
                k=False
                break  #break一定要有
        if k==True:
         cnt+=1
         ls.append(i)
    return ls

print(get_prime(101,200))

