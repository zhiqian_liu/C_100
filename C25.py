'''求1+2!+3!+...+20!的和。'''
#求阶乘
from functools import reduce
def factorial(n):
    return reduce(lambda x,y:x*y,range(1,n+1))
#求和
sum=0
for i in range (1,21):
    sum+=factorial(i)
print("计算结果为%d"%sum)







