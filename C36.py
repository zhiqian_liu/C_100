'''求100之内的素数'''
import math
def is_prime(n):#判断素数
    if n<=1:
        return 0
    else:
        for i in range (2,int(math.sqrt(n))+1):
            if n%i==0:
                return 0
        return 1
#确定范围并打印素数
def get_prime(a,b):
    for i in range(a,b-1):
        if is_prime(i):
            print(i,end=' ')

get_prime(1,100)


