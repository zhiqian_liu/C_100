'''输入某年某月某日，判断这一天是这一年的第几天？'''
#判断闰年
def is_leap_year(year):
    if (year%4==0 and year%100!=0) or (year%400==0):
        return 1
    else:
        return 0

def getdays(year,month,day):
    days=0
    month_list=[0,30,28,31,30,31,30,31,31,30,31,30,31]
    if is_leap_year(year):
        month_list[2]=29
#计算以月为单位的天数
    for i in range(month):
        days+=month_list[i]
#加上天数
    days+=day
    return days

year,month,day=map(int,input().split(','))
days_diff=getdays(year,month,day)
print('这是今年的第{}天'.format(days_diff))



