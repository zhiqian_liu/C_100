"""编写一个函数，输入n为偶数时，调用函数求1/2+1/4+...+1/n,
当输入n为奇数时，调用函数1/1+1/3+...+1/n"""
from fractions import Fraction
def sum_up(n):
    result=0
    if n<=0:
        print("输入不规范！")
    elif n%2==0:
        for i in range (2,n+1,2):
            result+=Fraction(1,i)
    else:
        for j in range (1,n+1,2):
            result+=Fraction(1,j)
    return result

n=int(input("请输入n:"))
print("计算结果为%.2f"%sum_up(n))

