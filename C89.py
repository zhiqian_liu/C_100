"""某个公司采用公用电话传递数据，数据是四位的整数，在传递过程中是加密的，
加密规则如下： 每位数字都加上5,然后用和除以10的余数代替该数字，
再将第一位和第四位交换，第二位和第三位交换"""
#生成密码
def get_encrypted(s):
    for i in range(4):
        s[i]=(s[i]+5)%10
    s[0],s[3]=s[3],s[0]
    s[1],s[2]=s[2],s[1]
    return s
#输入密码
s=[int(input("请输入数据：") )for i in range(4)]
print("加密后的数据为{}".format(get_encrypted(s)))
