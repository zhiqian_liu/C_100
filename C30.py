'''判断回文数。比如12321是回文数'''
def is_symmetry(s):
    if len(s)==1:
        return 1
    elif s==s[::-1]: #核心：采用反转法
        return 1
    else:
        return 0

s=str(input("请输入数字："))
if is_symmetry(s):
    print("这个数字是回文数！")
else:
    print("这个数字不是回文数！")

