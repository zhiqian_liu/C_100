'''一个整数，它加上100后是一个完全平方数，再加上168又是一个完全平方数，请问该数是多少？'''
import math

def intNum(n):
    k=int(math.sqrt(n)) #对n开根号后取整，如果n不是平方数，n!=k*k
    return n==k*k

for i in range(-100,1000000):   #注意是从-100开始，右边取一个足够大的数即可
    if intNum(i+100) and intNum(i+268):
        print('该数为：%d'%i)

