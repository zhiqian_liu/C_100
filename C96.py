"""计算字符串中子串出现的次数"""
#计算次数
def get_result(strings,x):
    cnt=0
    for s in strings:
        if s==x:
            cnt+=1
    return cnt
#输入输出
strings=str(input("请输入字符串："))
x=str(input("请输入带查询子串："))
print("{0}在输入的字符串中一共出现了{1}次~".format(x,get_result(strings,x)))

