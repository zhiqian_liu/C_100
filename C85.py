"""判断一个素数能被几个9组成的数整除"""
import math as ma
#判断素数
def is_prime(x):
    k=True
    for i in range (2,int(ma.sqrt(x))+1):
        if x%i==0:
            k=False
            break
    return k

n=int(input("请输入一个素数："))
if is_prime(n):
    nine='9'    #字符串yyds
    while int(str(nine))%n!=0:
        nine+='9'
        if int(str(nine))%n==0:
            print("素数{0}能被{1}个9组成的数字整除".format(n,len(nine)))
else:
    print("输入的不是素数！")

