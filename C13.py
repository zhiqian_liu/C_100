'''打印出所有的"水仙花数"
所谓"水仙花数"是指一个三位数
其各位数字立方和等于该数本身。
例如：153是一个"水仙花数"，因为153=1的三次方＋5的三次方＋3的三次方'''
def is_narcissus(x):    #判断水仙花数
    i=x//100
    j=x//10%10
    k=x%10
    if x==pow(i,3)+pow(j,3)+pow(k,3):
        return 1
    else:
        return 0

def get_narcissus(n,m): #获取水仙花数
    ls=[]
    for i in range(n,m+1):
        if is_narcissus(i):
            ls.append(i)
    return ls

n,m=map(int,input('请输入范围：').split(','))
print('从{0}到{1}范围内的水仙花数为{2}'.format(n,m,get_narcissus(n,m)))