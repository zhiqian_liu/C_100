"""有五个学生，每个学生有3门课的成绩，
从键盘输入以上数据（包括姓名，三门课成绩），
计算出平均成绩，将原有的数据和计算出的平均分数存放"""
dict={}
for i in range(5): #一共5个学生
    name=input("输入姓名：")
    arry=input("请输入成绩：")
    scores=[int(n) for n in arry.split(" ")]
    average_score=sum(scores)//len(scores) #求各科平均分
    dict[name]=average_score
#输入学生姓名查找对应平均分
s=input("输入学生姓名以查找平均分：")
print("学生{0}的各科平均分为{1}".format(s,dict[s]))
