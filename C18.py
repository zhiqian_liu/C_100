'''求s=a+aa+aaa+aaaa+aa...a的值，其中a是一个数字。
例如2+22+222+2222+22222(此时共有5个数相加)
几个数相加有键盘控制'''
def get_result(a,n): #a为数字，一共n个数相加
    sum=0
    for i in range(1,n+1):
        a1 = 0    #a1在哪里清零很重要
        for j in range(i):
            a1+=a*pow(10,j)
        sum+=a1
    return sum

a,n=map(int,input().split(','))
print("result=%d"%get_result(a,n))

