'''字符串反转，如将字符串 "www.runoob.com" 反转为 "moc.boonur.www"'''
s=input("输入字符串：")
#切片
print(s[::-1])
#reversed函数
print(reversed(s))#返回迭代器
print(list(reversed(s)))#需要制定类型才可具体输出
#reverse只能反转列表
ls=[]
for c in s:
    ls.append(c)
ls.reverse() #改变原有ls
print(ls)


