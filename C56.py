'''画圆'''
import math as m
import matplotlib.pyplot as plt

x=[]
y=[]
for a in range (1,11):
    for b in range (0,360):
        x.append(a*(m.cos(m.pi*(b/180))))
        y.append(a*(m.sin(m.pi*(b/180))))
plt.scatter(x,y,s=30)
plt.axis('scaled') #避免因比例而压缩为椭圆
plt.show()

