#兔子问题（斐波那契数列）
#斐波那契数列
def FibSeq(n):
    if n==0 or n==1:
        return n
    else:
        return FibSeq(n-1)+FibSeq(n-2)

months=int(input('请输入月数：'))
n=months//3
sum=0
for i in range(1,n+2):
    sum+=FibSeq(i)
print('第{0}月，兔子的总数为{1}只'.format(months,2*sum))



