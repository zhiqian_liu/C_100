'''一个数如果恰好等于它的因子之和，这个数就称为"完数"。
例如6=1＋2＋3.编程找出1000以内的所有完数'''
def get_factors(n): #获取因数
    i=2
    x=n
    ls=[1]
    while(i<=int(x/2)): #要用x存储n的值，因为判断条件不能变化
        if n%i==0:
            ls.append(i)
        i += 1
    return ls
#判断是否是完数，如果是，加到result里
result=[]
for i in range(2,1001): #范围从2开始，否则1也会算进完数里
    factors=get_factors(i)
    sum=0
    for j in range(0,len(factors)): #遍历范围为0~len(factors)-1
        sum+=factors[j]
    if sum==i:
        result.append(i)

#for循环本身是个迭代器对象,不加方括号就是输出迭代器对象本身，加了相当于又创建了一个list对象再输出
print('1000以内的完数为{}'.format([i for i in result]))
print('1000以内的完数为{}'.format(result))




