'''输入数组，最大的与第一个元素交换，最小的与最后一个元素交换，输出数组'''
def switch(ls):
    #存放最大值和最小值的下标
    max_index=0
    min_index=0
#最大和最小元素要分开遍历
    for i in range (len(ls)):
        if ls[max_index]<ls[i]:
            max_index=i
    ls[0], ls[max_index] = ls[max_index], ls[0]#最大元素和第一个元素换位置
    for j in range (len(ls)):
        if ls[min_index]>ls[j]:
            min_index = j
    ls[-1],ls[min_index]=ls[min_index],ls[-1]#最小元素和最后一个元素换位置
    return ls

ls=[1,2,9,3,-4,11,5]
print(ls)
print(switch(ls))



