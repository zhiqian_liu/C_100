'''利用递归函数调用方式，将所输入的5个字符，以相反顺序打印出来'''
#逆输出
def reverseStr(s,l,i):
    if l==0:
        return
    else:
        print(s[-i],end='')
        reverseStr(s,l-1,i+1)

s=input("请输入字符：")
l=len(s)
i=1
reverseStr(s,l,i)


