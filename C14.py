#将一个正整数分解质因数。例如：输入90,打印出90=2*3*3*5。
import math
#判断是否为素数
def is_prime(x):
    k=True
    for i in range(2,int(math.sqrt(x))): #int()不能少
        if x%i==0:
            k=False
            break
    return k

def get_factors(n): #获取质因数并打印
    i=2
    x=n
    while(i<int(math.sqrt(x))): #要用x存储n的值，因为判断条件不能变化
        if n%i==0:
            print(i,'*',sep='',end='')
            n=n//i
            if n // i == 1:
                print(n)  #最后一步打印的是n
                break
        else:
            i += 1



num=int(input())
if is_prime(num):
    print('素数不能分解！')
else:
    get_factors(num)
