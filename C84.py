"""一个偶数总能表示为两个素数之和"""
import math as ma
#判断素数
def is_prime(x):
    k=True
    for i in range (2,int(ma.sqrt(x))+1):
        if x%i==0:
            k=False
            break
    return k

n=int(input("请输入一个偶数："))
if n%2!=0 or n<=0:
    print("输入不规范！")
else:
    for i in range(n):
        for j in range(i,n): #从i开始而不是从0开始，保证不重复输出
            if n==i+j and (is_prime(i) and is_prime(j)):
                print("{0}可以分解为素数{1}和{2}之和".format(n,i,j))


