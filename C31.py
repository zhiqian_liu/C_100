'''请输入星期几的第一个字母来判断一下是星期几
如果第一个字母一样，则继续判断第二个字母'''
#默认第一个字母输入为大写，接下来字母输入为小写
dict_weekdays={'M':'Monday','W':'Wednesday','F':'Friday',
               'T':{'u':'Tuesday','h':'Thusday'},
               'S':{'a':'Saturday','u':'Sunday'}}
s1=input("Input the first letter: ")
if s1=='T' or s1=='S':
    s2=input("Input the second letter: ")
    print(dict_weekdays[s1][s2])
else:
    try:
        print(dict_weekdays[s1])
    except: #如果在字典里没有找到匹配
        print("error")

