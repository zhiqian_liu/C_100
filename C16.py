#输入两个正整数m和n，求其最大公约数和最小公倍数
#最大公约数
def gcd(x,y):
#获取最小值
    if x>y:
        smaller=y
    else:
        smaller=x
    for i in range(2,smaller+1):#for循环自动循环到满足条件的最大值
        if(x%i==0)and(y%i==0):
         res1=i
    return res1
#最小公倍数
def lcm(x,y):
    res2=(x//gcd(x,y))*y
    return res2

n,m=map(int,input('请输入两个正整数：').split(','))
if n<=0 or m<=0:
    print('您输入的数不符合规范！')
else:
    print('{0}和{1}的最大公约数为{2}，最小公倍数为{3}'.format(n,m,gcd(n,m),lcm(n,m)))
