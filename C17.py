#输入一行字符，分别统计出其中英文字母、空格、数字和其它字符的个数
s=input('输入一行字符：')
alpha=0 #统计字母个数
space=0 #统计空格
digit=0 #统计数字
others=0 #统计其他字符
for i in s:
    if i.isalpha():
        alpha+=1
    elif i.isdigit():
        digit+=1
    elif i.isspace():
        space+=1
    else:
        others+=1
print('中英文字母有{0}个，空格有{1}个，数字有{2}个，其他字符有{3}个'.format(alpha,space,digit,others))

