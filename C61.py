'''打印杨辉三角(打印出10行)'''
def yangHui(n):
   p=[1]
   print(p)
   while len(p)<=n-1: #第一行初始元素1，所以这里范围是n-1
       p=[1]+[p[i]+p[i+1] for i in range (len(p)-1)]+[1]
       print(p)     #逐行打印

n=int(input("请输入打印行数："))
yangHui(n)

