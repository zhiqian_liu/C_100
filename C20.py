'''一球从100米高度自由落下，每次落地后反跳回原高度的一半；
再落下，求它在第10次落地时，共经过多少米？第10次反弹多高？'''
def get_h_and_s(ori_h,n):
    ls=[]
    s=100  #变量一定要初始化
    h=0
    for i in range (1,n+1):
        h=0.5*ori_h
        s+=2*h
        ori_h/=2
    ls.append(h)
    ls.append(s)
    return ls

ori_h=int(input('请输入第一次下落高度：'))
n=int(input('反弹次数：'))
result=get_h_and_s(ori_h,n)
#以下两种不同输出方式要掌握
print('第{}次反弹高度为{:.2f}米,共经过{:.2f}米'.format(n,result[0],result[1]))
print('第%d次反弹高度为%.2f米,共经过%.2f米'%(n,result[0],result[1]))


