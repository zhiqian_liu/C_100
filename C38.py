'''求一个3*3矩阵对角线元素之和'''
import numpy as np
#求对角线元素之和
def s_DiagonalLine(ls,n):
    l=np.array(ls).reshape(n,n)
#ls是一个列表，要用arry(ls)将其变为数组才可用reshape函数
    sum=0
    for i in range (n):
        sum+=l[i,i]
    return sum

n=int(input("矩阵维数为："))
print("请按行输入矩阵的所有元素")
ls=[]
for i in range (n*n):
    ls.append(int(input()))
print("{0}维数组的主对角线元素之和为{1}".format(n,s_DiagonalLine(ls,n)))



